//
//  CustomParameterEncoding.swift
//  Rx-TestAssignmant
//
//  Created by Roman Volkov on 03/10/2017.
//  Copyright © 2017 Roman Volkov. All rights reserved.
//

import Foundation
import Moya
import Alamofire

final class CustomParameterEncoding: ParameterEncoding {
    fileprivate static let keyConst = "dataArray"
    
    func encode(_ urlRequest: URLRequestConvertible, with parameters: Parameters?) throws -> URLRequest {
        var request = try urlRequest.asURLRequest()
        
        if let items = (parameters?[CustomParameterEncoding.keyConst] as? [String]),
            items.count > 0 {
            var params = items.reduce("") { result, item in
                return "\(result)\(item)&"
            }
            params.removeLast()
            if let path = request.url?.absoluteString {
                request.url = URL(string: "\(path)?\(params)")
                print("==========================")
                print(request.url)
                print("==========================")
            }
        }
        
        return request
    }
    
    static var instance: CustomParameterEncoding {
        return CustomParameterEncoding()
    }
}

extension CustomParameterEncoding {
    static func toParameters(fromPoints array: [CGPoint], with key: String) -> Parameters {
        let textValues = array.map { "\(key)=\($0.x),\($0.y)" }
        return [CustomParameterEncoding.keyConst : textValues]
    }
}
