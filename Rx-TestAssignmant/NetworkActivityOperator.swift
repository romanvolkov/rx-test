//
//  NetworkActivityOperator.swift
//  Rx-TestAssignmant
//
//  Created by Roman Volkov on 03/10/2017.
//  Copyright © 2017 Roman Volkov. All rights reserved.
//

import Foundation
import RxSwift

extension Observable {
    func withNetworkingIndicator() -> Observable<Element> {
        return Observable<Element>.create { observer in
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            return self.observeOn(MainScheduler.instance).subscribe { element in
                switch element {
                case .next(let value):
                    observer.onNext(value)
                case .error(let error):
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    observer.onError(error)
                case .completed:
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    observer.onCompleted()
                }
            }
        }
    }
}
