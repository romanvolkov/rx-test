//
//  ViewController.swift
//  Rx-TestAssignmant
//
//  Created by Roman Volkov on 03/10/2017.
//  Copyright © 2017 Roman Volkov. All rights reserved.
//

import UIKit
import RxSwift
import Moya

final class ViewController: UIViewController {
    
    private let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK: - Rx task
    @IBAction func onTap(sender: AnyObject) {
        let provider = MoyaProvider<TestProvider>()
        provider.rx.request(.main).observeOn(MainScheduler.asyncInstance)
            .asObservable()
            .withNetworkingIndicator()
            .subscribe(onNext: { response in
                print(response)
            }, onError: { error in
                print("ERROR: \(error)")
            }, onCompleted: {
                print("completed")
            })
            .disposed(by: self.disposeBag)
    }
    
    //MARK: - Moya task
    @IBAction func onSecondTap(sender: AnyObject) {
        let endpointClosure = { (target: TestProvider) -> Endpoint<TestProvider> in
            let responseClosure = { () -> (EndpointSampleResponse) in
                return .networkResponse(200, target.sampleData)
            }
            return Endpoint<TestProvider>(url: URL(target: target).absoluteString,
                                          sampleResponseClosure: responseClosure,
                                          method: target.method,
                                          task: target.task)
        }
        
        let points: [CGPoint] = [CGPoint(x: 1, y: 2), CGPoint(x: 12, y: 23)]
        let provider = MoyaProvider<TestProvider>(endpointClosure: endpointClosure)
        provider.rx.request(.api(points))
            .asObservable()
            .subscribe(onNext: { response in
                print("==========================")
                print(response.request?.url)
                print("==========================")
            })
            .disposed(by: self.disposeBag)
    }
}

