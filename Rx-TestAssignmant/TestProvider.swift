//
//  TestProvider.swift
//  Rx-TestAssignmant
//
//  Created by Roman Volkov on 03/10/2017.
//  Copyright © 2017 Roman Volkov. All rights reserved.
//

import Foundation
import Moya

enum TestProvider {
    case main
    case api([CGPoint])
}

extension TestProvider: TargetType {
    var baseURL: URL { return URL(string: "https://swiftweekly.github.io")! }
    
    var path: String {
        switch self {
        case .main:
            return "/feed.xml"
        case .api:
            return "/"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .main:
            return .get
        case .api:
            return .get
        }
    }
    
    var sampleData: Data {
        return "sample text".data(using: .utf8)!
    }
    
    var task: Task {
        switch self {
        case .main:
            return .requestPlain
        case .api(let points):
            return .requestParameters(parameters: CustomParameterEncoding.toParameters(fromPoints: points, with: "points"),
                encoding: CustomParameterEncoding.instance)
        }
    }
    
    var validate: Bool {
        return true
    }
    
    var headers: [String: String]? {
        return [:]
    }
}
